from pyramid.view import view_config
from pyramid.i18n import TranslationStringFactory

_ = TranslationStringFactory(domain='MyProject')

@view_config(route_name='home', renderer='templates/mytemplate.pt')
def my_view(request):
    hello = _('Hi')
    return {'project':'MyProject',
            'hello':hello,}
